package com.sunbeaminfo.sh.hb.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.ParameterMode;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.transform.ResultTransformer;

import com.sunbeaminfo.sh.hb.entities.Book;
import com.sunbeaminfo.sh.hb.utils.HbUtil;

public class BookDao {
	public Book getBook(int id) {
		Session session = HbUtil.getSession();
		return session.get(Book.class, id);
	}
	public void addBook(Book b) {
		Session session = HbUtil.getSession();
		session.persist(b);
	}
	public void updateBook(Book b) {
		Session session = HbUtil.getSession();
		session.update(b);
	}
	public void deleteBook(int id) {
		Session session = HbUtil.getSession();
		Book b = getBook(id);
		if(b != null)
			session.remove(b);
	}
	public List<Book> getAuthorBooks(String author) {
		Session session = HbUtil.getSession();
		Criteria cr = session.createCriteria(Book.class);
		cr.add(Restrictions.eq("author", author));
		return cr.list();
	}
	public List<Book> getBooksByCriteria(DetachedCriteria dcr) {
		Session session = HbUtil.getSession();
		Criteria cr = dcr.getExecutableCriteria(session);
		return cr.list();
	}
	public List<String> getAuthors() {
		Session session = HbUtil.getSession();
		String sql = "SELECT DISTINCT AUTHOR FROM BOOKS";
		NativeQuery q = session.createSQLQuery(sql);
		return q.getResultList();
	}
	public List<Book> getBooks() {
		Session session = HbUtil.getSession();
		String hql = "from Book b";
		Query q = session.createQuery(hql);
		return q.getResultList();
	}
	public List<Book> getSubjectBooks(String subject) {
		Session session = HbUtil.getSession();
		Query q = session.getNamedQuery("hqlSubjectBooks");
		q.setParameter(1, subject);
		return q.getResultList();
	}
	public List<Book> getSubjectAuthorBooks(String subject, String author) {
		Session session = HbUtil.getSession();
		String hql = "from Book b where b.subject=:p_subject and b.author=:p_author";
		Query<Book> q = session.createQuery(hql);
		q.setParameter("p_subject", subject);
		q.setParameter("p_author", author);
		return q.getResultList();
	}
	public List<String> getSubjects() {
		Session session = HbUtil.getSession();
		Query q = session.getNamedQuery("hqlSubjects");
		return q.getResultList();
	}
	
	public List<Book> getBooksInfo1() {
		Session session = HbUtil.getSession();
		String hql = "select b.id, b.name, b.price from Book b";
		Query<Object[]> q = session.createQuery(hql);
		List<Book> list = new ArrayList<Book>();
		List<Object[]> rows = q.getResultList();
		for (Object[] cols : rows) {
			Book b = new Book();
			b.setId((Integer)cols[0]);
			b.setName((String)cols[1]);
			b.setPrice((Double)cols[2]);
			list.add(b);
		}
		return list;
	}
	
	public List<Book> getBooksInfo2() {
		Session session = HbUtil.getSession();
		String hql = "select b.id, b.name, b.price from Book b";
		Query q = session.createQuery(hql);
		q.setResultTransformer(new ResultTransformer() {
			@Override
			public Object transformTuple(Object[] tuple, String[] aliases) {
				Book b = new Book();
				b.setId((Integer)tuple[0]);
				b.setName((String)tuple[1]);
				b.setPrice((Double)tuple[2]);
				return b;
			}
			@Override
			public List transformList(List list) {
				return list;
			}
		});
		return q.getResultList();
	}

	public List<Book> getBooksInfo3() {
		Session session = HbUtil.getSession();
		String hql = "select new Book(b.id, b.name, b.price) from Book b";
		Query<Book> q = session.createQuery(hql);
		return q.getResultList();
	}
	
	public Map<String, Double> getSubjectwiseBooksAvgPrice() {
		Session session = HbUtil.getSession();
		String hql = "select b.subject, avg(b.price) from Book b group by b.subject";
		Query<Object[]> q = session.createQuery(hql);
		Map<String, Double> result = new HashMap<String, Double>();
		for (Object[] tuple : q.getResultList())
			result.put((String)tuple[0], (Double)tuple[1]);
		return result;
	}

	public int increaseBookPriceOfSubject(String subject) {
		Session session = HbUtil.getSession();
		String hql = "update Book b set b.price = b.price + b.price * 0.05 where b.subject=:p_subject";
		Query q = session.createQuery(hql);
		q.setParameter("p_subject", subject);
		return q.executeUpdate();
	}
	
	public int deleteBooksOfSubject(String subject) {
		Session session = HbUtil.getSession();
		String hql = "delete from Book b where b.subject=:p_subject";
		Query q = session.createQuery(hql);
		q.setParameter("p_subject", subject);
		return q.executeUpdate();
	}

	public List<Book> getBooksByAuthor(String author) {
		Session session = HbUtil.getSession();
		NativeQuery<Book> q = session.getNamedNativeQuery("sp_getbooks");
		q.setParameter("p_author", author);
		return q.getResultList();
	}
	
	// hibernate 3
	public Double getBookPrice(int id) {
		Session session = HbUtil.getSession();
		/***
		session.doWork(new Work() {
			@Override
			public void execute(Connection con) throws SQLException {
				// use con to call sp.
			}
		});
		***/
		Double result = session.doReturningWork(new ReturningWork<Double>() {
			@Override
			public Double execute(Connection con) throws SQLException {
				try(CallableStatement stmt = con.prepareCall("CALL SP_GET_PRICE(?,?)")) {
					stmt.setInt(1, id);
					stmt.registerOutParameter(2, Types.DOUBLE);
					stmt.execute();
					return stmt.getDouble(2);
				}
			}
		});
		return result;
	}
	
	// hibernate 5
	public Double getBookPrice5(int id) {
		Session session = HbUtil.getSession();
		ProcedureCall sp = session.createStoredProcedureCall("SP_GET_PRICE");
		sp.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter(1, Double.class, ParameterMode.OUT);
		sp.setParameter(0, id);
		sp.execute();
 		return (Double)sp.getOutputParameterValue(1);
	}
}







