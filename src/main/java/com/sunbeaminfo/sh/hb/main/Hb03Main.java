package com.sunbeaminfo.sh.hb.main;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.sunbeaminfo.sh.hb.daos.BookDao;
import com.sunbeaminfo.sh.hb.entities.Book;
import com.sunbeaminfo.sh.hb.utils.HbUtil;

public class Hb03Main {
	public static void main(String[] args) {
		/*
		SessionFactory factory = HbUtil.getSessionFactory();
		
		Session session1 = factory.getCurrentSession();
		Session session2 = factory.getCurrentSession();
		System.out.println("Is Same Session : " + (session1==session2));
		
		Session session3 = factory.openSession();
		Session session4 = factory.openSession();
		System.out.println("Is Same Session : " + (session3==session4));
		session3.close();
		session4.close();
		
		factory.close();
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			Book b = dao.getBook(11);
			System.out.println("Found : " + b);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			List<Book> list = dao.getAuthorBooks("Kanetkar");
			for (Book b : list)
				System.out.println(b);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			DetachedCriteria dcr = DetachedCriteria.forClass(Book.class);
			dcr.add(Restrictions.or(
					Restrictions.eq("author", "Kanetkar"), 
					Restrictions.eq("subject", "Java")
				)
			);
			dcr.addOrder(Order.desc("price"));
			List<Book> list = dao.getBooksByCriteria(dcr);
			for (Book b : list)
				System.out.println(b);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			List<String> list = dao.getAuthors();
			System.out.println(list);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			List<Book> list = dao.getBooks();
			for (Book b : list)
				System.out.println(b);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			List<Book> list = dao.getSubjectBooks("OS");
			for (Book b : list)
				System.out.println(b);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			List<Book> list = dao.getSubjectAuthorBooks("C", "Kanetkar");
			for (Book b : list)
				System.out.println(b);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			List<String> list = dao.getSubjects();
			System.out.println(list);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/

		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			List<Book> list = dao.getBooksInfo1();
			for (Book b : list)
				System.out.println(b);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			List<Book> list = dao.getBooksInfo2();
			for (Book b : list)
				System.out.println(b);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			List<Book> list = dao.getBooksInfo3();
			for (Book b : list)
				System.out.println(b);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			Map<String, Double> map = dao.getSubjectwiseBooksAvgPrice();
			for (Entry<String, Double> e : map.entrySet())
				System.out.println(e.getKey() + " => " + e.getValue());
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			int cnt = dao.increaseBookPriceOfSubject("Novell");
			System.out.println("Books updated : " + cnt);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			int cnt = dao.deleteBooksOfSubject("Novell");
			System.out.println("Books deleted : " + cnt);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			List<Book> list = dao.getBooksByAuthor("Kanetkar");
			for (Book b : list)
				System.out.println(b);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
 			Double price = dao.getBookPrice(11);
 			System.out.println("Book Price : " + price);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
 			Double price = dao.getBookPrice5(11);
 			System.out.println("Book Price : " + price);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		HbUtil.shutdown();
	}
}
